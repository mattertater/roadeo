// Socket/Express setup
var express = require('express');
var app = express();
app.use(express.static(__dirname + '/public'));
var server = app.listen(6969);
var io = require('socket.io').listen(server);

// Current player arrays
var clients = [];
var players = [];
var scores = [];
var allTimeHigh = [];

io.on('connection', newConnection);

// Used to not draw goal close to edges
var margin = 20;

// The closest the goal can spawn to any player
var minGoalDist = 200;

// Size of all players
var playerSize = 13;

// Size of canvas
var width = 600,
  height = 600;

var lastTime = 0,
  currentTime = 0;


// Player data
function Player(id, name, color, px, py) {
  this.id = id;
  this.name = name;
  this.color = color;
  this.score = 0;
  this.x = px;
  this.y = py;
}

// Goal data
var gx = 300,
  gy = 300;




// Obstacle data
function Obstacle(x, y, size) {

}
var obstacles = [];
var maxObstacles = 5;

// Power-up Data
function PowerUp(x, y, type) {

}
var powerups = [];
var maxPowerups = 2;




// Round/break data
// Times defined in seconds, converted based around this in program
var inRound = false;
var roundTime = 60;
var breakTime = 20;
var timerStart = Date.now();

// End first round manually, then it'll do it by itself
var timer = setTimeout(roundBreakCycle, (breakTime * 1000));


//
// A better log function, now with timestamp!
//
function log(message) {
  console.log(new Date().toLocaleString("en-US", {
    "timezone": "America/New_York",
    "hour12": "h12"
  }) + " ~~ " + message);
}


//
// Runs when new player enters the game, before name entry
// --Only adds to client array--
//
function newConnection(socket) {
  log("New Connection: " + socket.id);

  // New ID listing in clients
  clients.push(socket.id);

  // Send players id back to them
  socket.emit('id', {
    id: socket.id,
  });

  // Send list of current players to check name conflicts on client side
  socket.emit('initialPlayerData', {
    players: players,
  });

  socket.emit('goalData', {
    x: gx,
    y: gy,
  });

	socket.emit('allTimeHighScores', {
		allTimeHighScores: allTimeHigh,
	});

  // Runs when the client disconnects from the server
  socket.on('disconnect', function() {

    // Remove from client array
    var i = clients.indexOf(socket.id);
    clients.splice(i, 1);

    // Remove from players array
    i = players.map(function(e) {
      return e.id;
    }).indexOf(socket.id);

    if (i >= 0) {
      log("Removing player: " + players[i].name);
      io.emit('newMessage', {
        name: players[i].name,
        nameColor: players[i].color,
        message: " has left the game"
      });
      // Remove from players array
      players.splice(i, 1);
    } else {
      // Player probably never entered a name, or something else went wrong
      log("Someone left without logging in");
    }
  });


  // Runs when a player name is received from the client
  socket.on('nameEntered', function(data) {
    players.push(new Player(data.id, data.name, data.color, data.x, data.y));
    log("Added " + data.name + " as a player with ID " + data.id);
    io.emit('newMessage', {
      name: data.name,
      nameColor: data.color,
      message: " is now playing"
    });
  });


  // Runs whenever new position is received from the client
  socket.on('playerData', function(playerData) {
    // Get index of player
    var i = players.map(function(e) {
      return e.id;
    }).indexOf(playerData.id);

    // Make sure the index generated is valid
    if (i >= 0) {
      // Update player info in players array
      players[i].x = playerData.x;
      players[i].y = playerData.y;
    }
  });


  // Runs when the player is colliding with the goal
  socket.on('playerPoint', function(data) {

    // Player calculates and sends new goal position, sends to server
    gx = data.x;
    gy = data.y;

    // Get index of player
    var i = players.map(function(e) {
      return e.id;
    }).indexOf(data.id);

    log(players[i].id + " got a point!");

    if (i >= 0) {
      players[i].score++;
      sortPlayers();
    } else {
      log("Bad player index of " + i + " while incrementing point");
    }

    log("Sending new goal pos of " + gx + ", " + gy + " to all players");
    io.emit('goalData', {
      x: gx,
      y: gy,
    });

    log("Sending starburst with color " + data.color + " to pos " + data.sx + ", " + data.sy);
    io.emit('starBurst', {
      x: data.sx,
      y: data.sy,
      color: data.color,
    });
  });


  // Runs when the player wants an update of the players array and time data
  socket.on('requestUpdate', function() {

    // Send each of these twice in case one of them gets lost
    socket.emit('updatedPlayers', {
      players: players,
    });
    socket.emit('updatedPlayers', {
      players: players,
    });

    socket.emit('timeData', {
      inRound: inRound,
      timeLeft: getTimeLeft(),
    });
    socket.emit('timeData', {
      inRound: inRound,
      timeLeft: getTimeLeft(),
    });
  });
}


//
// Sorts the player array by score
//
function sortPlayers() {
  players.sort(function(a, b) {
    var val1 = a.score,
      val2 = b.score;
    if (val1 < val2) return 1;
    if (val1 > val2) return -1;
    return 0;
  });
}


//
// Calculate distance between to points, (x1, y1) and (x2, y2)
//
function distance(x1, y1, x2, y2) {
  var dist;
  dist = Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
  return dist;
}

//
// Calculate time left in a timer
// Math.floor makes it so you never see '0:00',
// which is the expected behavior
//
function getTimeLeft() {
  var timePassed = Math.floor((Date.now() - timerStart) / 1000);
  if (inRound)
    var timeLeft = roundTime - timePassed;
  else
    var timeLeft = breakTime - timePassed;
  return timeLeft;
}

//
// Reset player scores at start of new round
//
function removeScores() {
  for (var i = 0; i < players.length; i++) {
    players[i].score = 0;
  }
}

//
// Cycles between round and break
//
function roundBreakCycle() {
  // if a round was just happening, then go to break
  if (inRound) {
    log("entering break");
    timerStart = Date.now();
    inRound = false;
    updateHighScores();
    timer = setTimeout(roundBreakCycle, (breakTime * 1000));
  } else { // if a break was just happening, start a new round
    log("entering round");
    timerStart = Date.now();
    removeScores();
    io.emit('goalData', {
      x: 300,
      y: 300,
    });
    gx = 300;
    gy = 300;
    inRound = true;
    timer = setTimeout(roundBreakCycle, (roundTime * 1000));
  }
}


//
// Update the overall high score list
//
function updateHighScores() {
  // add players
  for (var i = 0; i < players.length; i++) {
    allTimeHigh.push(players[i]);
  }

  // consolodate
  var toRemove = [];
  for (var i = 0; i < allTimeHigh.length - 1; i++) {
    for (var j = i + 1; j < allTimeHigh.length; j++) {
      if (allTimeHigh[i].name == allTimeHigh[j].name) {
        if (allTimeHigh[i].score > allTimeHigh[j].score)
          toRemove.push(j);
        else
          toRemove.push(i);
      }
    }
  }
  for (var i = 0; i < toRemove.length; i++) {
		allTimeHigh.splice(toRemove[i], 1);
  }

  // sort
  allTimeHigh.sort(function(a, b) {
    var val1 = a.score,
      val2 = b.score;
    if (val1 < val2) return 1;
    if (val1 > val2) return -1;
    return 0;
  });

	// push to client
	io.emit('allTimeHighScores', {
		allTimeHighScores: allTimeHigh,
	});
}


//
// Find spot to place obstacle/, avoiding all other objects
//
function findSpot(type) {

  var x, y;

  // Obstacles are squares so calculations will be different
  if (type == "obstacle") {} else {

  }
}