// Variable initialization
var canvas = document.querySelector("#roadeo");
canvas.width = canvas.height = 600;
var context = canvas.getContext('2d');
context.font = '1em Helvetica';

// Setting up sockets
var socket = io.connect(window.location.href);

// Get our ID back from the server
socket.on('id', function (data) {
  playerID = data.id;
});

// Get list of names so we know if ours is legal
socket.on('initialPlayerData', function (players) {
  for (var i = 0; i < players.players.length; i++) {
    nameList.push(players.players[i].name);
  }
});

// When new goal data is received
socket.on('goalData', function (data) {
  gx = data.x;
  gy = data.y;
});

// When new time data is received
socket.on('timeData', function (data) {
  inRound = data.inRound;
  timeLeft = data.timeLeft;
})

// When new player data is received
socket.on('updatedPlayers', function (data) {
  players = data.players;
});

// When a new message is received
socket.on('newMessage', function (data) {
  messageNames.unshift(data.name);
  messageNameColors.unshift(data.nameColor);
  messages.unshift(data.message);
});

// Get position and color of starburst, make that shit pop
socket.on('starBurst', function (data) {
  starBurst(data.x, data.y, data.color);
});

socket.on('allTimeHighScores', function (data) {
  allTimeHigh = data.allTimeHighScores;
})

// Game-changing variables
var maxSpeed = 5;
var force = 2;
var friction = 0.97;
var wallBounce = -0.7; // -1 preserves all velocity

// Player variables
var players = [];
var allTimeHigh = [];
var playerID;
var name = '';
var nameList = [];
var messages = [];
var messageNames = [];
var messageNameColors = [];
var playerSize = 13;
var playerColor = getHAkronColor();
var playerBorderColor = modifyAlpha(playerColor, -0.5);
var px = 100,
  py = 100;
var vx = 0,
  vy = 0;

// Goal variables
var gx, gy;
var goalColor = '#FFDF00';
var goalOutlineColor = '#D4AF37';
var goalSize = 20;
var margin = 20;
var minGoalDist = 200;

// Data variables
var keys = [];

// Nipple variables
var rad = 0;
var mag = 0;

// Time/Round variables
var timeLeft = 0;
var inRound = true;

// TODO: change these colors to something fun still, but not HAkron specific
// HAkron 3000 branding
var starWhite = "#F7F5E2";
var darkBlue = "#070E20";
var mainBlue = "#1F2F5A";
var shadowBlue = "#0B1838"
var accentPink = "#BE70AE"
var lightBlue = "#28A7D7"
var moonLight = "#E6E1CE"
var moonDark = "#C0C0C0"
var planetLight = "#F26B37"
var planetDark = "#D73027"

// TODO: stars aren't necessary, maybe keep cause they look nice?
//       do i even still want a black background?
// Particle / stars stuff
var particles = [];
var stars = [];
createStars(200);


//
// Get player name
//
function nameEnter(id) {
  name = document.getElementById('username').value;

  if (!name) {
    alert("Please enter a name");
  } else if (document.getElementById('username').value.length > 11) {
    alert("Please enter a name less than 10 characters in length");
    name = '';
  } else if (nameList.includes(document.getElementById('username').value)) {
    alert("User with that name already exists");
    name = '';
  } else {
    // Valid name was enetered,send new player info to server
      socket.emit('nameEntered', {
        id: playerID,
        name: name,
        color: playerColor,
        x: px,
        y: py,
      });
      $('#nameModal').modal('hide');
    }
}

// Handle pressing enter in the text field
document.getElementById('username').onkeypress = function (e) {
  if (!e) e = window.event;
  var keyCode = e.keyCode || e.which;
  if (keyCode == '13')
    nameEnter();
}


//
// Listen for key presses/releases
//
window.addEventListener("keydown", keysPressed, false);
window.addEventListener("keyup", keysReleased, false);

gameLoop();


//
// Main game loop where functions are called
//
function gameLoop() {

  socket.emit('requestUpdate', {});
  requestAnimationFrame(gameLoop);

  // reset canvas and draw background
  clearCanvas();
  drawStars();

  if (inRound) {

    // draw all other elements to the screen
    drawGoal();
    drawAllPlayers();
    drawParticles();
    drawScoreboard();
    drawMessages();

    // Check if mobile
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
      getNip();
    }

    if (name) {
      // do player calculations and movement checking
      updateVelocities();
      movePlayer();
      checkCollisions();


      // Send player data to server
      socket.emit('playerData', {
        id: playerID,
        x: px,
        y: py,
      });

    }
  } else { // reset player velocity and position in break
    drawBreakScreen();
    vx = 0;
    vy = 0;
    py = 100;
    px = 100;
  }
  drawTimer();
}


//
// Nipple movement
//
function getNip() {
  manager.on('move dir', function (evt, nip) {
    rad = nip.angle.radian;
    if ((nip.force / 8) > 1)
      mag = 1;
    else
      mag = (nip.force / 8);
  });

  manager.on('end', function (evt, nip) {
    mag = 0;
  });
}


//
// New movement update: calculate force based on active joystick / key presses
//
function updateVelocities() {
  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
    var forceX = mag * Math.cos(rad);
    var forceY = mag * Math.sin(rad);

    if (forceX > 1)
      forceX = 1;
    else if (forceX < -1)
      forceX = -1;

    if (forceY > 1)
      forceY = 1;
    else if (forceY < -1)
      forceY = -1;

    if (hypot(vx, vy) < maxSpeed) {
      vx += forceX;
      vy -= forceY;
    }
  } else {
    // ←
    if (keys[37] || keys[65]) {
      if (vx > -maxSpeed * 0.9)
        vx -= 0.4;
    }

    // ↑
    if (keys[38] || keys[87]) {
      if (vy > -maxSpeed * 0.9)
        vy -= 0.4;
    }

    // →
    if (keys[39] || keys[68]) {
      if (vx < maxSpeed * 0.9)
        vx += 0.4;

    }

    // ↓
    if (keys[40] || keys[83]) {
      if (vy < maxSpeed * 0.9)
        vy += 0.4;
    }
  }
}


//
// Calculates new player position
//
function movePlayer() {
  // Apply friction to slow things down real nice
  vy *= friction;
  vx *= friction;

  // Bounds checking

  // If player touching right or left wall
  if (px > (canvas.width - playerSize)) {
    px = canvas.width - playerSize;
    vx *= wallBounce;
  } else if (px < (0 + playerSize)) {
    px = playerSize;
    vx *= wallBounce;
  }

  // If player touching bottom or top wall
  if (py > (canvas.height - playerSize)) {
    py = canvas.height - playerSize;
    vy *= wallBounce;
  } else if (py < (0 + playerSize)) {
    py = playerSize;
    vy *= wallBounce;
  }

  // Change position
  px += vx;
  py += vy;
}


//
// Checks to see if the player is colliding with the goal
//

function checkCollisions() {
  // Collision checking
  if (distance(gx, gy, px, py) < (goalSize + playerSize)) {

    // find (about) point where they intersected to produce starburst
    var sx = ((gx - px) / 2) + px;
    var sy = ((gy - py) / 2) + py;

    // Send new players array to client to update scores
    resetGoal();
    socket.emit('playerPoint', {
      id: playerID,
      x: gx,
      y: gy,
      sx: sx,
      sy: sy,
      color: playerColor,
    });
  }
}


//
// Draws the goal coin, and checks to see if the player is in it
//
function drawGoal() {
  // Reset opacity just in case
  context.globalOpacity = 1.0;

  // Center colored part
  context.beginPath();
  context.arc(gx, gy, goalSize, 0, 2 * Math.PI);
  context.fillStyle = goalColor;
  context.fill();

  // Slightly off-color outline
  context.beginPath();
  context.arc(gx, gy, goalSize, 0, 2 * Math.PI);
  context.strokeStyle = goalOutlineColor;
  context.lineWidth = 2;
  context.stroke();
}


//
// Draws all players on screen, calls other draw functions
//
function drawAllPlayers() {
  var x, y, color;

  for (var i = 0; i < players.length; i++) {

    // Temporary variables for readability
    x = players[i].x;
    y = players[i].y;
    color = players[i].color;
    context.globalAlpha = 1.0;

    // Center colored part
    context.beginPath();
    context.arc(x, y, playerSize, 0, 2 * Math.PI);
    context.fillStyle = color;
    context.fill();

    // Player outline
    context.beginPath();
    context.arc(x, y, playerSize, 0, 2 * Math.PI);
    context.strokeStyle = modifyAlpha(color, -0.5);;
    context.lineWidth = 2;
    context.stroke();

    // Player name
    drawText(players[i].name, x - 20, y - 20, "1em Helvetica", starWhite);

    // Reset opacity
    context.globalAlpha = 1.0;
  }
}


//
// Draw particles of star burts
//
function drawParticles() {
  for (var i = 0; i < particles.length; i++) {
    if (particles[i] != null)
      particles[i].draw(i);
  }
}

function drawStars() {
  for (var i = 0; i < stars.length; i++) {
    if (stars[i] != null) {
      stars[i].draw();
    }
  }
}


//
// Draws the player names and scores in the top right
//
function drawScoreboard() {

  // Header
  drawText("Player", 10, 20, "1em Helvetica", starWhite);
  drawText("Score", 110, 20, "1em Helvetica", starWhite);
  drawText("- - - - - - - - - - - - - - -", 10, 30, "1em Helvetica", starWhite);

  // Increments for each player, putting each under the last
  var offset = 0;

  // Cycle through players, draw name and score
  for (var i = 0; i < players.length; i++) {
    drawText(players[i].name, 10, 50 + offset, "1em Helvetica", players[i].color);
    drawText(players[i].score, 110, 50 + offset, "1em Helvetica", players[i].color);
    offset += 20;
  }
  context.fillStyle = starWhite;

  // Debugging stats
  //  context.fillText("vx: " + vx, 200, 20);
  //  context.fillText("vy: " + vy, 200, 40);
  //
  //  context.fillText("rad: " + rad, 200, 70);
  //  context.fillText("mag: " + mag, 200, 90);
}


//
// Draws the timer whether you're in a round or not
//
function drawTimer() {

  // format that time all nice and string like
  if (timeLeft == 60)
    var timeLeftFormatted = "1:00";
  else if (timeLeft < 10)
    var timeLeftFormatted = "0:0" + timeLeft;
  else
    var timeLeftFormatted = "0:" + timeLeft;

  // draw timer in top right if a round is going on
  if (inRound)
    drawText("Time left in round", 440, 20, "1.3em Helvetica", starWhite);
  else
    drawText("Time until next round starts", 360, 20, "1.3em Helvetica", starWhite);

  drawText(timeLeftFormatted, 525, 45, "2em Helvetica", accentPink);

  context.font = '1em Helvetica';
  context.fillStyle = starWhite;
}

//
// Prints out the game updates in the bottom left corner
//
function drawMessages() {

  // Reset opacity and position offset
  context.globalAlpha = 1;
  var offset = 0;
  var nameWidth = 0;

  // Loop through messages, print with different offsets and opacities
  for (var i = 0; i < messages.length; i++) {
    if (messages[i]) {

      // draw colored name
      drawText(messageNames[i], 10, 580 - offset, "1em Helvetica", messageNameColors[i]);

      // draw the rest of the message offset by width of name
      nameWidth = context.measureText(messageNames[i]).width;
      drawText(messages[i], 10 + nameWidth, 580 - offset, "1em Helvetica", starWhite);

      // increment offset, decrement opacity
      offset += 20;
      context.globalAlpha -= 0.2;
    } else {
      console.log("Error with message " + i + ": " + messages[i]);
    }
  }
  context.globalAlpha = 1;
}


//
// Increments position if a key is pressed, handles multiple keypresses
// and support WASD as well as arrow keys
//
function keysPressed(e) {
  // Only be able to move if there is a name
  if (name) {
    // Store entry if key is pressed
    keys[e.keyCode] = true;
  }
}


//
// Shows scores of last game, message saying its a break
// and the next round will start soon
//
function drawBreakScreen() {
  //messages = [];
  var listOffset = 0;
  var place = 1;

  drawText("High scores of all time:", 40, 150, "2.5em Helvetica", starWhite);

  // draw high score list
  for (var i = 0; i < allTimeHigh.length; i++) {
    if (place == 1) {
      context.font = "1.7em Helvetica";
      var XOffset = -10;
      var YOffset = 10;
    } else if (place == 2) {
      context.font = "1.4em Helvetica";
      var XOffset = -5;
      var YOffset = 5;
    } else if (place == 3) {
      context.font = "1.2em Helvetica";
      var XOffset = -3;
      var YOffset = 3;
    } else {
      context.font = "1em Helvetica";
      var XOffset = 0;
      var YOffset = 0;
    }

    context.fillStyle = allTimeHigh[i].color;
    context.fillText(place + ". " + allTimeHigh[i].score + " - " + allTimeHigh[i].name, 250 + XOffset, 200 + listOffset);

    listOffset += 20;
    listOffset += YOffset;
    place++;
  }
}


//
// Set value to false in array of keys if released
//
function keysReleased(e) {
  keys[e.keyCode] = false;
}


//
// Create random 6-digit hex value
//
function getRandomColor() {
  var digits = '0123456789ABCDEF';
  var color = '#';

  for (var i = 0; i < 6; i++) {
    color += digits[Math.floor(Math.random() * 16)];
  }
  return color;
}


//
// Gets a player color, HAkron 3000 style
//
function getHAkronColor() {
  var colors = ["#28A7D7", "#E6E1CE", "#F26B37", "#D73027", "#ACF39D", "#F4E409",
    "#E56399", "#FF6978", "#ECE8EF", "#7B1E7A", "#F18F01", "#FFB400", "#59CD90"
  ];
  var good = false;
  var attempts = 0;
  var choice = "";
  // Make sure we get a unique color
  do {
    choice = colors[Math.floor(Math.random() * colors.length)];
    for (var i = 0; i < players.length; i++) {
      if (choice == players[i].color) {
        break;
      } else if (i == players.length - 1)
        good = true;
    }
    attempts++;
    if (attempts > 100) // if all colors are taken, just copy one
      good = true;
  } while (!good);
  return choice;
}


//
// Resets the goal position and size
//
function resetGoal() {
  gPos = getGoalPosition();
  gx = gPos[0], gy = gPos[1];
}


//
// Set new goal position, min of 200 away from any player
//
function getGoalPosition() {
  // If nobody is playing, don't check based on players
  if (players.length == 0) return [300, 300];
  else {
    var x, y;
    var good = false;
    do {
      for (var i = 0; i < players.length; i++) {
        x = (Math.random() * (canvas.width - (2 * margin))) + margin;
        y = (Math.random() * (canvas.height - (2 * margin))) + margin;
        dist = distance(x, y, players[i].x, players[i].y);
        if (dist > minGoalDist) good = true;
        else good = false;
      }
    } while (!good);
    return [x, y];
  }
}


//
// Calculate distance between to points, (x1, y1) and (x2, y2)
//
function distance(x1, y1, x2, y2) {
  var dist;
  dist = Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
  return dist;
}


//
// Hypotenuse calculator
//
function hypot(w, h) {
  return Math.sqrt((w * w) + (h * h));
}


//
// Pretty self explanatory
//
function clearCanvas() {
  context.clearRect(0, 0, canvas.width, canvas.height);
}


//
// Brightens a color, percent is a range -1 to 1
// -1 for black, 1 for white
// I got it online
//
// Thanks internet
//
function modifyAlpha(color, percent) {
  var f = parseInt(color.slice(1), 16),
    t = percent < 0 ? 0 : 255,
    p = percent < 0 ? percent * -1 : percent,
    R = f >> 16,
    G = f >> 8 & 0x00FF,
    B = f & 0x0000FF;
  return "#" + (0x1000000 + (Math.round((t - R) * p) + R) * 0x10000 + (Math.round((t - G) * p) + G) * 0x100 + (Math.round((t - B) * p) + B)).toString(16).slice(1);
}


//
// Draw text function, make it easy to use certain font, fill style, etc.
//
function drawText(text, x, y, font, color) {
  context.fillStyle = color;
  context.font = font;
  context.fillText(text, x, y);
}


//
// Particle object, used for drawing explosions when a player scores
//
function Particle(x, y, color) {
  this.x = x;
  this.y = y;
  this.size = Math.random() * 2 + 0.75;
  this.color = modifyAlpha(color, (Math.random() * 0.5 - 0.2));
  this.vx = Math.random() * 10 - 5;
  this.vy = Math.random() * 10 - 5;
  this.life = Math.random() * 20;
  this.draw = function (id) {
    this.x += this.vx;
    this.y += this.vy;

    this.yx *= 0.95;
    this.vy *= 0.95;

    // Age the particle
    this.life--;

    // If Particle is old, it goes in the chamber for renewal
    if (this.life <= 0) {
      particles.splice(id, 1);
    }

    context.fillStyle = this.color;
    context.fillRect(this.x, this.y, this.size, this.size);
  }
}


//
// Make a burst of colored particles
//
function starBurst(x, y, color) {
  for (var i = 0; i < 100; i++) {
    particles.push(new Particle(x, y, color));
  }
}


//
// Just some nice background twinkling stars
//
function Star(x, y) {
  this.x = x;
  this.y = y;
  this.size = Math.random() * 2;
  this.opacity = 0.2;
  this.draw = function () {
    // change opacity a bit for a little twinkle
    var delta = Math.random() * 0.25 - 0.125;
    this.opacity += delta;

    // draw the star
    context.globalAlpha = this.opacity;
    context.fillRect(this.x, this.y, this.size, this.size);

    // reset opacity
    context.globalAlpha = 1.0;
  }
}

function createStars(num) {
  for (var i = 0; i < num; i++) {
    stars.push(new Star(Math.random() * canvas.width, Math.random() * canvas.height));
  }
}


function roundRect(x, y, width, height, radius, color) {
  if (typeof radius === 'undefined') {
    radius = 5;
  }
  if (typeof radius === 'number') {
    radius = {
      tl: radius,
      tr: radius,
      br: radius,
      bl: radius
    };
  } else {
    var defaultRadius = {
      tl: 0,
      tr: 0,
      br: 0,
      bl: 0
    };
    for (var side in defaultRadius) {
      radius[side] = radius[side] || defaultRadius[side];
    }
  }
  context.fillStyle = color;
  context.beginPath();
  context.moveTo(x + radius.tl, y);
  context.lineTo(x + width - radius.tr, y);
  context.quadraticCurveTo(x + width, y, x + width, y + radius.tr);
  context.lineTo(x + width, y + height - radius.br);
  context.quadraticCurveTo(x + width, y + height, x + width - radius.br, y + height);
  context.lineTo(x + radius.bl, y + height);
  context.quadraticCurveTo(x, y + height, x, y + height - radius.bl);
  context.lineTo(x, y + radius.tl);
  context.quadraticCurveTo(x, y, x + radius.tl, y);
  context.closePath();
  context.fill();

}
